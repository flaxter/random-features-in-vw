#!/bin/sh
data="data/" # change to the path to your data directory
vw --bfgs -k --l2 .01 --cache_file $data/impensData.txt.vw.cache $data/impensData.txt.vw -f impensClassifier --passes 200 --loss_function logistic # -q :: 
vw --bfgs -k --l2 .01 --cache_file $data/schillingData.txt.vw.cache $data/schillingData.txt.vw -f schillingClassifier --passes 200 --loss_function logistic # -q :: 
vw --bfgs -k --l2 .01 --cache_file $data/1625Data.txt.vw.cache $data/1625Data.txt.vw -f 1625Classifier --passes 200 --loss_function logistic # -q ::
vw --bfgs -k --l2 .01 --cache_file $data/746Data.txt.vw.cache $data/746Data.txt.vw -f 746Classifier --passes 200 --loss_function logistic # -q ::

