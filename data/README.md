Original source: 

https://archive.ics.uci.edu/ml/datasets/HIV-1+protease+cleavage

Thorsteinn Rögnvaldsson, Liwen You and Daniel Garwicz, 'State of the art prediction of HIV-1 protease cleavage sites' Bioinformatics (2015) 31 (8): 1204-1210. doi: 10.1093/bioinformatics/btu810, First published online: December 9, 2014


