#!/bin/sh
data="data/" # change to the path to your data directory

## Use impens to predict the others
vw -t -i impensClassifier -p results/impens_impens_predictions $data/impensData.txt.vw --loss_function logistic
vw -t -i impensClassifier -p results/impens_schilling_predictions $data/schillingData.txt.vw --loss_function logistic
vw -t -i impensClassifier -p results/impens_1625_predictions $data/1625Data.txt.vw --loss_function logistic
vw -t -i impensClassifier -p results/impens_746_predictions $data/746Data.txt.vw --loss_function logistic

echo "After training on impens, AUC for testing on other datasets:"
python accuracy.py $data/746Data.txt  results/impens_746_predictions
python accuracy.py $data/1625Data.txt  results/impens_1625_predictions
python accuracy.py $data/schillingData.txt  results/impens_schilling_predictions
python accuracy.py $data/impensData.txt  results/impens_impens_predictions

##  Use schilling to predict the others
vw -t -i schillingClassifier -p results/schilling_impens_predictions $data/impensData.txt.vw --loss_function logistic
vw -t -i schillingClassifier -p results/schilling_schilling_predictions $data/schillingData.txt.vw --loss_function logistic
vw -t -i schillingClassifier -p results/schilling_1625_predictions $data/1625Data.txt.vw --loss_function logistic
vw -t -i schillingClassifier -p results/schilling_746_predictions $data/746Data.txt.vw --loss_function logistic

echo "After training on schilling, AUC for testing on other datasets:"
python accuracy.py $data/746Data.txt  results/schilling_746_predictions
python accuracy.py $data/1625Data.txt  results/schilling_1625_predictions
python accuracy.py $data/schillingData.txt  results/schilling_schilling_predictions
python accuracy.py $data/impensData.txt  results/schilling_impens_predictions

## Use 1625 to predict the others 
vw -t -i 1625Classifier -p results/1625_impens_predictions $data/impensData.txt.vw --loss_function logistic
vw -t -i 1625Classifier -p results/1625_schilling_predictions $data/schillingData.txt.vw --loss_function logistic
vw -t -i 1625Classifier -p results/1625_1625_predictions $data/1625Data.txt.vw --loss_function logistic
vw -t -i 1625Classifier -p results/1625_746_predictions $data/746Data.txt.vw --loss_function logistic

echo "After training on 1625, AUC for testing on other datasets:"
python accuracy.py $data/746Data.txt  results/1625_746_predictions
python accuracy.py $data/1625Data.txt  results/1625_1625_predictions
python accuracy.py $data/schillingData.txt  results/1625_schilling_predictions
python accuracy.py $data/impensData.txt  results/1625_impens_predictions

## Use 746 to predict the others
vw -t -i 746Classifier -p results/746_impens_predictions $data/impensData.txt.vw --loss_function logistic
vw -t -i 746Classifier -p results/746_schilling_predictions $data/schillingData.txt.vw --loss_function logistic
vw -t -i 746Classifier -p results/746_1625_predictions $data/1625Data.txt.vw --loss_function logistic
vw -t -i 746Classifier -p results/746_746_predictions $data/746Data.txt.vw --loss_function logistic

echo "After training on 746, AUC for testing on other datasets:"
python accuracy.py $data/746Data.txt  results/746_746_predictions
python accuracy.py $data/1625Data.txt  results/746_1625_predictions
python accuracy.py $data/schillingData.txt  results/746_schilling_predictions
python accuracy.py $data/impensData.txt  results/746_impens_predictions

