import sys
import numpy as np

if len(sys.argv) < 4:
    print "Usage: %s filename number_of_random_features random_seed"%(sys.argv[0])
    print "If you want to train on one dataset and test on another (as described in HIV1_PR_documentation.txt) you _MUST_ generate the same set of random features, so pick a random_seed to use to generate both datasets."
    exit(1)

alphabet = 'ARNDCQEGHILKMFPSTWYV'
mapping = {}
for i,a in enumerate(alphabet):
    m = [0]*20
    m[i] = 1
    mapping[a] = np.array(m)

f = open(sys.argv[1],'r')
D = int(sys.argv[2])
np.random.seed(int(sys.argv[3]))
w = np.random.multivariate_normal([0]*20,np.identity(20),size=D)
C = np.sqrt(2.0 / D)
def feature_map(x,label):
    z = np.dot(w,x)
    return ' '.join(['%s%d:%f'%(label,i+1,z) for i,z in enumerate(np.concatenate((C*np.cos(z),C*np.sin(z))))])


namespaces = 'ABCDEFGH'
for row in f:
    x,y = row.rstrip().split(',')
    covars = [feature_map(mapping[r],namespaces[i].lower()) for i, r in enumerate(x)]
    print y, ' '.join(['|%s %s'%(namespaces[i],covars[i]) for i in range(8)])
