Author: Seth Flaxman <flaxman [at] stats.ox.ac.uk>

Date: 7 December 2015

This is a little tutorial about using Random Fourier Features with Vowpal Wabbit.

First, make sure you have a working copy of [Vowpal Wabbit](https://github.com/JohnLangford/vowpal_wabbit/wiki) (henceforth "vw").

Now, we'll generate some Random Fourier Features to do classification with the [HIV-1 protease cleavage dataset](https://archive.ics.uci.edu/ml/datasets/HIV-1+protease+cleavage). Rögnvaldsson et al. [[2015](http://bioinformatics.oxfordjournals.org/content/31/8/1204.short)] use a linear SVM and get very good performance. Hopefully we can do as well.

According to the documentation, the dataset consists of strings of length 8 and binary labels, and apparently the letters in these strings can take 20 values, so they are usually mapped into a 160-dimensional binary representation. We'll adopt that representation, except we'll add some structure so that we can consider interactions between entries in the string.

To generate the random Fourier features we use the [cos(wx),sin(wx)] representation (see [Sutherland and Schneider 2015](http://arxiv.org/abs/1506.02785) for a discussion). This means to generate an approximate feature map phi(x) we sample w_1, ..., w_D from a Gaussian distribution and concatenate:

\phi(x) = \sqrt(2/D) [cos(w_1 x), sin(w_1 x), ..., cos(w_d x), sin(w_d x)].

Now the format for VW can be a little tricky, because whitespace matters (except when it doesn't) and you can leave things out (except when you can't). When in doubt, try the [VW data format validator](http://hunch.net/~vw/validate.html) 
The python script generate_features.py takes as input any of the data files, and outputs a properly formatted VW input file. For now, generate_features.py uses RBF kernels with length-scale 1.

How do I induce structure? Each of the 8 letters in the string gets mapped to a different namespace in VW, e.g. here is an instance (with label -1 meaning it is a negative example) in VW's format (with only D = 2 random features):

-1 |A b1:-0.664164 b2:0.014285 b3:0.747587 b4:-0.999898 |B b1:-0.664164 b2:0.014285 b3:0.747587 b4:-0.999898 |C b1:-0.664164 b2:0.014285 b3:0.747587 b4:-0.999898 |D b1:0.474404 b2:0.244902 b3:-0.880307 b4:-0.969548 |E b1:-0.974095 b2:0.846673 b3:-0.226141 b4:-0.532113 |F b1:0.015315 b2:0.999840 b3:-0.999883 b4:-0.017909 |G b1:0.474404 b2:0.244902 b3:-0.880307 b4:-0.969548 |H b1:0.474404 b2:0.244902 b3:-0.880307 b4:-0.969548

There are 8 namespaces, labeled with the capital letters A-H. We take a letter ('A'), map it to a unary representation (x = 00000000000000000001), and then
evaluate \phi(x) which gives us a 4 dimensional vector (since we have the cosine and the sine terms). We label the predictors in this vector b1, b2, b3, and b4.


Instructions:

1) The shell script generate_features.sh calls generate_features.py for each dataset. Run:

```
#!bash

sh generate_features.sh 
```
2) To train VW, run train.sh. I've only spent a little bit of time non-systematically playing with tuning parameters. Probably grid search / cross-validation is the way to go here. As far as I know, there's nothing built into VW to facilitate this, but it's not too hard to write a python/bash script to do it.

```
#!bash
sh train.sh
```

3) To test, run:


```
#!bash

sh test.sh > results/results.txt
```

Here are the results I get, which are on par with the ones in Rögnvaldsson et al.:


```
              Test data  746         1625        Schilling   Impens
   Train data
   746                   0.985       0.967       0.874*      0.843*
   1625                  0.983*      0.998       0.875*      0.810
   Schilling             0.940*      0.937*      0.980       0.908
   Impens                0.877       0.872       0.929       0.967

* indicates state-of-the-art performance
```

**Postscript**

I picked this dataset because I was expecting to get some performance improvements by considering interaction terms, which is very easy to do in VW using
the "-q" option and the namespaces, e.g. -q AB will include all interactions between features in namespace A and features in namespace B. "-q ::" will include ALL interactions.
But on this dataset, I don't see any performance improvements. Another important area to explore is different kernels and different length-scales. In this case, a linear SVM is nearly state-of-the-art and the data is categorical, but for real-valued or a mix of real and structured data the question of kernel choice (and interactions between kernels) becomes much more interesting.
