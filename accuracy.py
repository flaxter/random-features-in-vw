import sys
from sklearn import metrics
import numpy as np

true = np.array([float(row.split(',')[1]) for row in open(sys.argv[1],'r').read().split()])
score = np.array([float(f) for f in open(sys.argv[2],'r').read().split()])
print 'AUC for %s: %.03f'%(sys.argv[1], metrics.roc_auc_score(true, score))

#predictions = score
#predictions[predictions < 0] = -1
#predictions[predictions >= 0] = 1
#print 'Accuracy for %s: %.03f'%(sys.argv[1], np.mean(predictions == true))
